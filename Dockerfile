FROM scratch
ADD ca-certificates.crt /etc/ssl/certs/
ADD nepremecnine_scanner /
CMD ["/nepremecnine_scanner"]
