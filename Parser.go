package main

import (
	"github.com/PuerkitoBio/goquery"
	"log"
	"strings"
)

func GetPage(url, otype string, htype string, place string) {
		body := GetRequestSimple(url)
		r := strings.NewReader(string(body))
		doc, err := goquery.NewDocumentFromReader(r)
		if err != nil {
			log.Fatal(err)
		}


		doc.Find(".oglas_container").Each(func(i int, s *goquery.Selection) {
			var rooms string
			var link string
			var nadstropje string
			var leto string
			var velikost string
			var info string
			var price string
			var agency string
			s.Find(".tipi").Each(func(i int, s *goquery.Selection) {
				rooms = s.Text()
			})
			s.Find(".slika").Each(func(i int, s *goquery.Selection) {
				link, _ = s.Attr("href")
			})
			s.Find(".atribut").Each(func(i int, s *goquery.Selection) {
				if i == 0 {
					nadstropje = s.Text()
				}
			})
			s.Find(".atribut.leto").Each(func(i int, s *goquery.Selection) {
				leto = s.Text()
			})
			s.Find(".velikost").Each(func(i int, s *goquery.Selection) {
				velikost = s.Text()
			})
			s.Find(".kratek_container").Each(func(i int, s *goquery.Selection) {
				info = strings.TrimSpace(strings.Replace(s.Text(), "\n", "", -1))
			})
			s.Find(".cena").Each(func(i int, s *goquery.Selection) {
				price = s.Text()
			})
			s.Find(".agencija").Each(func(i int, s *goquery.Selection) {
				agency = s.Text()
			})

			if AddOffer("https://www.nepremicnine.net/"+link, otype, htype, place, rooms, nadstropje, leto, velikost, info, price, agency) {

			}

		})

}
