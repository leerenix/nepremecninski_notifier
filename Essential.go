package main

import (
	"database/sql"
	"fmt"
	"github.com/ddliu/go-httpclient"
	_ "github.com/lib/pq"
	"io/ioutil"
	"log"
	"strconv"

	"strings"
)


var db, err = sql.Open("postgres", BdURL)

var replacer = strings.NewReplacer("'", "")
var replacer2 = strings.NewReplacer(".", "")

func GetRequestSimple(url string) []byte {
	resp, err := httpclient.
		NewHttpClient().
		Begin().
		WithHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36").
		WithOption(httpclient.OPT_TIMEOUT, 60).
		Get(url)
	if err != nil {
		fmt.Println(err)
	} else {

		if resp.StatusCode != 404 {
			body, _ := ioutil.ReadAll(resp.Body)
			resp.Body.Close()
			return body
		}

	}
	return nil
}

func initDB() {
	var err error
	db, err = sql.Open("postgres", BdURL)
	if err != nil {
		panic(err)
	} else {
		fmt.Println("DB connection finished")
	}
}

func DeleteUserPref(id int64, pref string) {
	sqlReq := "UPDATE public.users SET " + pref + " = '' WHERE tg_id = " + strconv.Itoa(int(id)) + ";"
	_, err := db.Query(sqlReq)
	if err != nil {
		fmt.Println(err)
	}
}

type User struct {
	id             int
	tg_id          int
	oddaja_types   sql.NullString
	places         sql.NullString
	house_types    sql.NullString
	lang           int
	settings_stage int
}

func GetUsers() []User {
	var ret []User
	sqlReq := "SELECT * FROM public.users;"
	answer, err := db.Query(sqlReq)
	if err != nil {
		fmt.Println(err)

	} else {
		for answer.Next() {
			var locThread User
			if err := answer.Scan(&locThread.id, &locThread.tg_id, &locThread.oddaja_types, &locThread.places, &locThread.house_types, &locThread.lang, &locThread.settings_stage); err != nil {
				log.Fatal(err)
			}
			ret = append(ret, locThread)
		}
	}
	return ret
}

func UpdateUserTypePref(id int64, pref string, value string) {
	sqlReq := "SELECT " + pref + " FROM public.users WHERE tg_id = " + strconv.Itoa(int(id)) + ";"
	res, err := db.Query(sqlReq)
	if err != nil {
		fmt.Println(err)
	}
	var prefValue sql.NullString
	for res.Next() {
		if err := res.Scan(&prefValue); err != nil {
			fmt.Println(err)
		}
	}
	reqString := prefValue.String + "," + value
	reqString =  strings.ToLower(reqString)
	sqlReq = "UPDATE public.users SET " + pref + " = '" + reqString + "' WHERE tg_id = " + strconv.Itoa(int(id)) + ";"
	res, err = db.Query(sqlReq)
	if err != nil {
		fmt.Println(err)
	}
}

func Split(url string) []string {
	var ret []string
	ret = strings.SplitAfter(url, "/")
	return ret
}

func AddOffer(link string, otype string, htype string, place string, rooms string, nadstropje string, leto string, velikost string, info string, price string, agency string) bool {
	response := false
	if err != nil {
		fmt.Println(err)
	} else {
		sqlReq := "INSERT INTO public.offers (link, o_type, h_type, place, rooms, nadstropje, year, velikost, info, price, agency) VALUES ('" + link + "', '" + otype + "','" + htype + "','" + place + "', '" + rooms + "', '" + nadstropje + "', '" + leto + "', '" + velikost + "', '" + info + "', '" + price + "', '" + agency + "');"
		answer, err := db.Query(sqlReq)
		if err != nil {
			if strings.Contains(err.Error(), "duplicate key") {
			} else {
				fmt.Println(err)
			}
		} else {
			fmt.Println("INSERTED")
			err = answer.Close()
			if err != nil {
				fmt.Println("Close ERROR: " + err.Error())
			}
		}

	}

	return response
}


func GetUnsentOffers(otype []string, htype []string, place []string) []string {
	var ret []string



	sqlOtype := ""
	sqlHtype := ""
	sqlPlace := ""
	for _, item := range otype {
		if item != "" && item != "," {
			sqlOtype = sqlOtype + "','" + "oglasi-"+item+"/"
		}
	}
	for _, item := range htype {
		if item != "" && item != "," {
			sqlHtype = sqlHtype + "','" + item
		}
	}
	for _, item := range place {
		if item != "" && item != "," {
			sqlPlace = sqlPlace + "','" + item + "/"
		}
	}

	sqlReq := "SELECT link FROM public.offers where sent = false and place IN ( '" + sqlPlace + "' ) and o_type IN ( '" + sqlOtype + "' ) and h_type IN ('" + sqlHtype + "' );"

	sqlReq = strings.ToLower(sqlReq)
	answer, err := db.Query(sqlReq)
	if err != nil {
		fmt.Println(err)

	} else {
		for answer.Next() {
			var locThread string
			if err := answer.Scan(&locThread); err != nil {
				log.Fatal(err)
			}
			ret = append(ret, locThread)

		}
	}

	sqlReq = "UPDATE public.offers SET sent = true where sent = false;"
	_, err = db.Query(sqlReq)
	if err != nil {
		fmt.Println(err)

	}
	return ret
}

func CreateUser(id int, lang int, settings int) error {
	sqlReq := "INSERT INTO public.users (tg_id, lang, settings_stage) VALUES (" + strconv.Itoa(id) + ", " + strconv.Itoa(lang) + ", " + strconv.Itoa(settings) + ")"
	_,err := db.Query(sqlReq)
	if err != nil {
		fmt.Println(err)
		return err
	}
	return nil
}

func UpdateUserSettingsStage(id int, settings int) {
	sqlReq := "UPDATE public.users SET settings_stage = " + strconv.Itoa(settings) + " WHERE tg_id = " + strconv.Itoa(id) + ";"
	_,err := db.Query(sqlReq)
	if err != nil {
		fmt.Println(err)
	}
}

func GetUserSettingsLevel(id int) int {
	sqlReq := "SELECT settings_stage FROM public.users WHERE tg_id = " + strconv.Itoa(id) + ";"
	res, err := db.Query(sqlReq)
	if err != nil {
		fmt.Println(err)
	}
	var stage int
	res.Next()
	if err := res.Scan(&stage); err != nil {
		fmt.Println(err)
	}

	return stage
}

func GetUserLang(id int) int {
	sqlReq := "SELECT lang FROM public.users WHERE tg_id = " + strconv.Itoa(id) + ";"
	res, err := db.Query(sqlReq)
	if err != nil {
		fmt.Println(err)
	}
	var stage int
	for res.Next() {
		if err := res.Scan(&stage); err != nil {
			fmt.Println(err)
		}
	}

	return stage
}

func CheckIfContains(stringOr string, stringSub string) bool {
	if strings.Contains(strings.ToLower(stringOr), stringSub) {
		return true
	} else {
		return false
	}
}
