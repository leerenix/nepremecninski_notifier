package main

import (
	"fmt"
	tgbotapi "gopkg.in/telegram-bot-api.v4"
	"log"
	"net/http"
)

func handleRequests(updates tgbotapi.UpdatesChannel, bot *tgbotapi.BotAPI) {
	for update := range updates {
		fmt.Println(update.Message.Text)

		var stage = GetUserSettingsLevel(int(update.Message.Chat.ID))
		fmt.Println(stage)
		if update.Message.Text == "Еще" || update.Message.Text == "More" {
			stage--
			UpdateUserSettingsStage(int(update.Message.Chat.ID), stage)
		} else if update.Message.Text == "Дальше" || update.Message.Text == "Next" {
			stage++
			UpdateUserSettingsStage(int(update.Message.Chat.ID), stage)
		}
		if update.Message.Text == "/start" || update.Message.Text == "/settings" {
			DeleteUserPref(update.Message.Chat.ID, "oddaj_types")
			DeleteUserPref(update.Message.Chat.ID, "places")
			DeleteUserPref(update.Message.Chat.ID, "house_types")
			UpdateUserSettingsStage(int(update.Message.Chat.ID), 0)
			//MSGLangPick(update, bot)
			SettingsStart(update, bot)
		} else if stage == 0 {
			SettingsStart(update, bot)
		} else if stage == 1 {
			SettingsType(update, bot)
		} else if stage == 2 {
			MSGSettingsTypeHouse(update, bot)
			stage++
			UpdateUserSettingsStage(int(update.Message.Chat.ID), stage)

		} else if stage == 3 {
			SettingsTypeHouse(update, bot)
		} else if stage == 4 {
			MSGSettingsPlace(update, bot)
			stage++
			UpdateUserSettingsStage(int(update.Message.Chat.ID), stage)
		} else if stage == 5 {
			SettingsPlace(update, bot)
		} else if stage == 6 {
			MSGDone(update, bot)
		}
	}
}

func MSGLangPick(update tgbotapi.Update, bot *tgbotapi.BotAPI) {
	UpdateUserSettingsStage(int(update.Message.Chat.ID), 0)
	msg := tgbotapi.NewMessage(update.Message.Chat.ID, "Lang/Язык/Jezik")
	msg.ReplyMarkup = AddLangKeyboard()
	bot.Send(msg)
}

func MSGDone(update tgbotapi.Update, bot *tgbotapi.BotAPI) {
	msg := tgbotapi.NewMessage(update.Message.Chat.ID, "Завершено")
	bot.Send(msg)
}

func SettingsStart(update tgbotapi.Update, bot *tgbotapi.BotAPI) {
	lang := GetUserLang(int(update.Message.Chat.ID))
	message := "ВЫбери тип:"
	if lang == 0 {
		message = "Pick type:"
	}

	CreateUser(int(update.Message.Chat.ID), 0, 0)
	//
	//
	//if update.Message.Text == "RU" {
	//	CreateUser(int(update.Message.Chat.ID), 1, 0)
	//
	//} else if update.Message.Text == "EN" {
	//	CreateUser(int(update.Message.Chat.ID), 0, 0)
	//
	//} else if update.Message.Text == "SLO" {
	//	CreateUser(int(update.Message.Chat.ID), 2, 0)
	//
	//}
	msg := tgbotapi.NewMessage(update.Message.Chat.ID, message)
	msg.ReplyMarkup = AddTypeKeyboard()
	bot.Send(msg)
	UpdateUserSettingsStage(int(update.Message.Chat.ID), 1)
}

func SettingsType(update tgbotapi.Update, bot *tgbotapi.BotAPI) {

	if update.Message.Text == "Oddaja" {
		UpdateUserTypePref(update.Message.Chat.ID, "oddaj_types", update.Message.Text)

	} else if update.Message.Text == "Prodaja" {
		UpdateUserTypePref(update.Message.Chat.ID, "oddaj_types", update.Message.Text)

	} else if update.Message.Text == "Nakup" {
		UpdateUserTypePref(update.Message.Chat.ID, "oddaj_types", update.Message.Text)

	} else if update.Message.Text == "Najem" {
		UpdateUserTypePref(update.Message.Chat.ID, "oddaj_types", update.Message.Text)
	}

	msg := tgbotapi.NewMessage(update.Message.Chat.ID, "Еще или дальше?")
	msg.ReplyMarkup = AddMoreOrNoKeyboard()
	bot.Send(msg)

}

func MSGSettingsTypeHouse(update tgbotapi.Update, bot *tgbotapi.BotAPI) {
	msg := tgbotapi.NewMessage(update.Message.Chat.ID, "Выбери тип дома:")
	msg.ReplyMarkup = AddSettingsHouseTypeKeyboard()
	bot.Send(msg)
}

func MSGSettingsPlace(update tgbotapi.Update, bot *tgbotapi.BotAPI) {
	msg := tgbotapi.NewMessage(update.Message.Chat.ID, "Выбери место:")
	msg.ReplyMarkup = AddSettingsPlaceKeyboard()
	bot.Send(msg)
}
func SettingsPlace(update tgbotapi.Update, bot *tgbotapi.BotAPI) {

	if update.Message.Text == "LJmesto" {
		UpdateUserTypePref(update.Message.Chat.ID, "places", update.Message.Text)

	} else if update.Message.Text == "LJokolica" {
		UpdateUserTypePref(update.Message.Chat.ID, "places", update.Message.Text)

	} else if update.Message.Text == "Gorenjska" {
		UpdateUserTypePref(update.Message.Chat.ID, "places", update.Message.Text)

	} else if update.Message.Text == "JPrimorska" {
		UpdateUserTypePref(update.Message.Chat.ID, "places", update.Message.Text)

	} else if update.Message.Text == "SPrimorska" {
		UpdateUserTypePref(update.Message.Chat.ID, "places", update.Message.Text)

	} else if update.Message.Text == "Notranjska" {
		UpdateUserTypePref(update.Message.Chat.ID, "places", update.Message.Text)

	} else if update.Message.Text == "Savinjska" {

		UpdateUserTypePref(update.Message.Chat.ID, "places", update.Message.Text)
	} else if update.Message.Text == "Podravska" {

		UpdateUserTypePref(update.Message.Chat.ID, "places", update.Message.Text)
	} else if update.Message.Text == "Koroška" {

		UpdateUserTypePref(update.Message.Chat.ID, "places", update.Message.Text)
	} else if update.Message.Text == "Dolenjska" {
		UpdateUserTypePref(update.Message.Chat.ID, "places", update.Message.Text)

	} else if update.Message.Text == "Posavska" {
		UpdateUserTypePref(update.Message.Chat.ID, "places", update.Message.Text)

	} else if update.Message.Text == "Zasavska" {
		UpdateUserTypePref(update.Message.Chat.ID, "places", update.Message.Text)

	} else if update.Message.Text == "Pomurska" {
		UpdateUserTypePref(update.Message.Chat.ID, "places", update.Message.Text)

	}
	msg := tgbotapi.NewMessage(update.Message.Chat.ID, "Еще или дальше?")
	msg.ReplyMarkup = AddMoreOrNoKeyboard()
	bot.Send(msg)
}

func SettingsTypeHouse(update tgbotapi.Update, bot *tgbotapi.BotAPI) {
	if update.Message.Text == "Stanovanje" {
		UpdateUserTypePref(update.Message.Chat.ID, "house_types", update.Message.Text)

	} else if update.Message.Text == "Hisa" {

		UpdateUserTypePref(update.Message.Chat.ID, "house_types", update.Message.Text)

	} else if update.Message.Text == "Vikend" {
		UpdateUserTypePref(update.Message.Chat.ID, "house_types", update.Message.Text)

	} else if update.Message.Text == "Posest" {
		UpdateUserTypePref(update.Message.Chat.ID, "house_types", update.Message.Text)

	} else if update.Message.Text == "Poslovni Prostor" {
		UpdateUserTypePref(update.Message.Chat.ID, "house_types", update.Message.Text)

	} else if update.Message.Text == "Garaza" {
		UpdateUserTypePref(update.Message.Chat.ID, "house_types", update.Message.Text)

	} else if update.Message.Text == "Pocitniski objekt" {
		UpdateUserTypePref(update.Message.Chat.ID, "house_types", update.Message.Text)

	}
	msg := tgbotapi.NewMessage(update.Message.Chat.ID, "Еще или дальше?")
	msg.ReplyMarkup = AddMoreOrNoKeyboard()
	bot.Send(msg)
}

func AddMoreOrNoKeyboard() tgbotapi.ReplyKeyboardMarkup {
	moreButton := tgbotapi.NewKeyboardButton("Еще")
	nextButton := tgbotapi.NewKeyboardButton("Дальше")
	var buttons []tgbotapi.KeyboardButton
	buttons = append(buttons, moreButton, nextButton)
	keyboad := tgbotapi.NewReplyKeyboard(buttons)
	return keyboad
}

func AddTypeKeyboard() tgbotapi.ReplyKeyboardMarkup {
	buttonOddaja := tgbotapi.NewKeyboardButton("Oddaja")
	buttonProdaja := tgbotapi.NewKeyboardButton("Prodaja")
	buttonNakup := tgbotapi.NewKeyboardButton("Nakup")
	buttonNajem := tgbotapi.NewKeyboardButton("Najem")
	var buttons []tgbotapi.KeyboardButton
	buttons = append(buttons, buttonOddaja, buttonProdaja, buttonNakup, buttonNajem)
	keyboad := tgbotapi.NewReplyKeyboard(buttons)
	return keyboad
}

func AddSettingsPlaceKeyboard() tgbotapi.ReplyKeyboardMarkup {

	LJmesto := tgbotapi.NewKeyboardButton("LJ-mesto")
	LJokolica := tgbotapi.NewKeyboardButton("LJ-okolica")
	Gorenjska := tgbotapi.NewKeyboardButton("Gorenjska")
	JPrimorska := tgbotapi.NewKeyboardButton("J. Primorska")
	SPrimorska := tgbotapi.NewKeyboardButton("S.Primorska")
	Notranjska := tgbotapi.NewKeyboardButton("Notranjska")
	Savinjska := tgbotapi.NewKeyboardButton("Savinjska")
	Podravska := tgbotapi.NewKeyboardButton("Podravska")
	Koroška := tgbotapi.NewKeyboardButton("Koroška")
	Dolenjska := tgbotapi.NewKeyboardButton("Dolenjska")
	Posavska := tgbotapi.NewKeyboardButton("Posavska")
	Zasavska := tgbotapi.NewKeyboardButton("Zasavska")
	Pomurska := tgbotapi.NewKeyboardButton("Pomurska")
	containerButton1 := tgbotapi.NewKeyboardButtonRow(LJmesto, LJokolica, Gorenjska)
	containerButton2 := tgbotapi.NewKeyboardButtonRow(JPrimorska, SPrimorska, Notranjska)
	containerButton3 := tgbotapi.NewKeyboardButtonRow(Savinjska, Podravska, Koroška)
	containerButton4 := tgbotapi.NewKeyboardButtonRow(Dolenjska, Posavska, Zasavska)
	containerButton5 := tgbotapi.NewKeyboardButtonRow(Pomurska)
	keyboad := tgbotapi.NewReplyKeyboard(containerButton1, containerButton2, containerButton3, containerButton4, containerButton5)
	return keyboad
}

func AddLangKeyboard() tgbotapi.ReplyKeyboardMarkup {
	buttonRu := tgbotapi.NewKeyboardButton("RU")
	buttonEn := tgbotapi.NewKeyboardButton("EN")
	buttonSlo := tgbotapi.NewKeyboardButton("SLO")
	var buttons []tgbotapi.KeyboardButton
	buttons = append(buttons, buttonSlo, buttonEn, buttonRu)
	keyboad := tgbotapi.NewReplyKeyboard(buttons)

	return keyboad
}

func AddSettingsHouseTypeKeyboard() tgbotapi.ReplyKeyboardMarkup {
	Stanovanje := tgbotapi.NewKeyboardButton("Stanovanje")
	Hisa := tgbotapi.NewKeyboardButton("Hisa")
	Vikend := tgbotapi.NewKeyboardButton("Vikend")
	Posest := tgbotapi.NewKeyboardButton("Posest")
	Prostor := tgbotapi.NewKeyboardButton("Poslovni Prostor")
	Garaza := tgbotapi.NewKeyboardButton("Garaza")
	objekt := tgbotapi.NewKeyboardButton("Pocitniski objekt")
	containerButton1 := tgbotapi.NewKeyboardButtonRow(Stanovanje, Hisa, Vikend, Posest)
	containerButton2 := tgbotapi.NewKeyboardButtonRow(Prostor, Garaza, objekt)
	keyboad := tgbotapi.NewReplyKeyboard(containerButton1, containerButton2)
	return keyboad
}

func TelegramInit(token string, webhook bool) {
	bot, err := tgbotapi.NewBotAPI(token)
	if err != nil {
		log.Panic(err)
	}


	bot.Debug = false

	log.Printf("Authorized on account %s", bot.Self.UserName)
	var updates tgbotapi.UpdatesChannel
	if webhook {
		_, err = bot.SetWebhook(tgbotapi.NewWebhook("https://bot.zelenin.top/" + bot.Token))
		if err != nil {
			log.Fatal(err)
		}
		info, err := bot.GetWebhookInfo()
		if err != nil {
			log.Fatal(err)
		}
		if info.LastErrorDate != 0 {
			log.Printf("Telegram callback failed: %s", info.LastErrorMessage)
		}
		updates = bot.ListenForWebhook("/" + bot.Token)

		go http.ListenAndServe("0.0.0.0:8100", nil)

	} else {

		u := tgbotapi.NewUpdate(0)
		u.Timeout = 3

		updates, err = bot.GetUpdatesChan(u)


	}

	go SendingCycle(bot)

	handleRequests(updates, bot)

}

func SendMsg(id int, msg string, bot *tgbotapi.BotAPI){

}