module NEPREMICNINE_Scanner

go 1.14

require (
	github.com/PuerkitoBio/goquery v1.5.1
	github.com/ddliu/go-httpclient v0.6.7
	github.com/jackc/pgx/v4 v4.8.1
	github.com/lib/pq v1.8.0
	gitlab.com/mediafetcher/module_telegram v0.0.0-20200728205206-171b63b063d6
	gopkg.in/telegram-bot-api.v4 v4.6.4
)
